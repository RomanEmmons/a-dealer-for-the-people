process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../backend/server/index');
const should = chai.should();

// Set up the chai Http assertion library
chai.use(chaiHttp);

// Test
describe('Reviews', () => {

    /**
     * GET /dealer/reviews
     */
	 describe('GET /dealer/reviews', () => {
        it('it should GET the top 3 overly positive offenders', (done) => {
            chai.request(server)
                .get('/dealer/reviews')
                .end((err, res) => {
					if (err) {
						console.log(err);
						done();
					}
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(3);
                    done();
                });
        });
    });
});