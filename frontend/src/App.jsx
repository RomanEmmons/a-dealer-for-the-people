import React from 'react';
import ReactDOM from 'react-dom';
import Parent from './components/Parent.jsx'

ReactDOM.render(<Parent />, document.getElementById('app'));
