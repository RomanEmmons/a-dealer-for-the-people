const router = require('express').Router();
const HeadlessChromeScraper = require('../../services/puppeteer');

router.route('/reviews').get(async (req, res) => {

	const scraper = await new HeadlessChromeScraper();

	await scraper.scrape();

	const topThreeOffenders = scraper.getTopThreeOffenders();

	console.log('Top 3 Offenders: ', topThreeOffenders);

	res.send(topThreeOffenders);
});

module.exports = router;