const puppeteer = require('puppeteer');

module.exports = class HeadlessChromeScraper {
	constructor() {
		this.reviews = [];
		this.top3Offenders = [];
	}

	async scrape() {

		// puppeteer boilerplate
		const { page, browser } = await this.launchBrowser({
			args: ['--disable-features=site-per-process', '--disable-web-security', '--window-size=1300,800'],
			headless: true,
			slowMo: 10,
			defaultViewport: null
		});

		await page.setUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36');

		// loop through first 5 review pages
		for (let i = 1; i <= 5; i++) {
			// Navigate to login page.
			await page.goto(`https://www.dealerrater.com/dealer/McKaig-Chevrolet-Buick-A-Dealer-For-The-People-dealer-reviews-23685/page${i}/?filter=#link`);

			// scrape body from each review
			const reviewBodies = await page.$$eval(
			'.col-xs-12.col-sm-9.pad-none.review-wrapper > .tr.margin-top-md > .td.text-left.valign-top > .font-16.review-content.margin-bottom-none.line-height-25',
			rows => rows.map(row => {
				return row.innerText;
			}));

			// scrape reviewer name from each review
			const reviewers = await page.$$eval(
				'.col-xs-12.col-sm-9.pad-none.review-wrapper > .margin-bottom-sm.line-height-150 > span',
				rows => rows.map(row => {
					return row.innerText.slice(2, row.innerText.length);
			}));

			// scrape number of stars from each review
			const numberOfStars = await page.$$eval(
				'.col-xs-6.col-sm-12.pad-none.dealership-rating > .rating-static.visible-xs.pad-none.margin-none',
				rows => rows.map(row => {
					return parseInt(row.className.match(/[0-5]{1}/g)[0]);
			}));

			// format scraped data and add to reviews
			this.reviews = [...this.reviews, ...this.formatReviews(reviewBodies, reviewers, numberOfStars)];
		}

		await browser.close();
	}

	async launchBrowser(options) {
		// Initiate headless chrome session.
		const browser = await puppeteer.launch(options);
		const page = await browser.newPage();
		return { page, browser };
	}

	formatReviews(bodies, authors, stars) {
		let formattedReviewsArray = [];

		for (let i = 0; i < 10; i++) {
			// find all instances of an exclamation mark in review body
			const exclamationMarkArray = bodies[i].match(/!/g)

			formattedReviewsArray.push({
				body: bodies[i],
				author: authors[i],
				stars: stars[i],
				exclamations: exclamationMarkArray?.length === undefined ? 0 : exclamationMarkArray?.length
			})
		};

		return formattedReviewsArray;
	}

	// find top 3 overly positive reviews:
	// each must have 5 stars and the most exclamation marks in descending order
	getTopThreeOffenders() {
 		// sort by number of exclamation marks in body
		const sortedReviews = this.reviews.sort((a, b) =>  b.exclamations - a.exclamations);
		
		const topThreeOffenders = [];

		let currentIndex = 0;

		while (topThreeOffenders.length < 3) {

			if (sortedReviews[currentIndex].stars === 5) {
				topThreeOffenders.push(sortedReviews[currentIndex]);
			}

			currentIndex += 1;
		}

		return topThreeOffenders;
	}
} 
