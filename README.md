# A Dealer For the People
This application was built to identify the egregiously and utterly false overly positive reviews that are being posted on DealerRater.com about KBG outpost McKaig Chevrolet Buick. The most offensive reviews are decided by requiring a minimum of 5 stars and then sorted by the number of disingenuous exclamation marks contained in the review body in descending order. In order to identify the top three offenders, the user must clone this application to their local machine, `cd` into therepository, and enter `npm i`. Once the dependencies have finished installing, simply run `npm run test` to see the offenders logged in the terminal above the test results (it takes about 20 seconds to scrape and log the data). For ease of use, the Webpack bundle has already been pushed to GitLab, so there is no need to trigger the build.

Most of the magic happens in `/backend/services/puppeteer.js`. In that file, you will find the scraping and sorting logic. The test suite is a mock API GET request that runs through Express router using Mocha and Chai. In order to pass, the response must return status `200`, the response must be an array, and the array must have a length of three.

## Stack
- Node w/Express
- Mocha
- Chai
- Puppeteer

## Test
- `npm i` to install dependancies
- `npm run test` runs mocha testing suite
	- top 3 offenders will be logged in console above test results

## Start
- `npm i` to install dependancies
- `npm start` (you must run `npm run react:dev` at least one time prior in order to compile bundle before this will work)

## Dev Start
- `npm i` to install dependancies
- `npm run react:dev` triggers Webpack to compile bundle and hot reloading
- `npm run server:dev` starts Express server listening at port 3000 and hot reloading
- `.env` file containing DB credentials inside `/database` is required
	- DB_HOST=''
	- DB_USER=''
	- DB_PASSWORD=''
	- DB_NAME=''

## TODO


